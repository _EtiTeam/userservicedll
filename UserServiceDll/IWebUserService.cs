﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Xml;

namespace UserServiceDll
{
    // required 
    [ServiceContract]
    public interface IRestUserService
    {
        //Main Web Resources 
        [OperationContract]
        [WebGet(UriTemplate = "login.html")]
        System.IO.Stream getLoginPage();

        [OperationContract]
        [WebGet(UriTemplate = "assets/js/{jsFile}")]
        System.IO.Stream getJsFile(string jsFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/css/{cssFile}")]
        System.IO.Stream getCssFile(string cssFile);

        [OperationContract]
        [WebGet(UriTemplate = "images/{imageFile}")]
        System.IO.Stream getImageFile(string imageFile);

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff?v=4.6.3")]
        System.IO.Stream getFont1();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.woff2?v=4.6.3")]
        System.IO.Stream getFont2();

        [OperationContract]
        [WebGet(UriTemplate = "assets/fonts/fontawesome-webfont.ttf?v=4.6.3")]
        System.IO.Stream getFont3();
              

        //AJAX
        [OperationContract]
        [WebInvoke(UriTemplate = "create", ResponseFormat = WebMessageFormat.Json)]
        bool Create(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(UriTemplate = "signin", ResponseFormat = WebMessageFormat.Json)]
        Guid? Login(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(UriTemplate = "logout", ResponseFormat = WebMessageFormat.Json)]
        void Logout(System.IO.Stream s); 

        [OperationContract]
        [WebInvoke(UriTemplate = "getUser", ResponseFormat = WebMessageFormat.Json)]
        ModelDLL.User getUser(System.IO.Stream s);

        [OperationContract]
        [WebInvoke(UriTemplate = "isSignedIn", ResponseFormat = WebMessageFormat.Json)]
        bool isSignedIn(System.IO.Stream s);

        [OperationContract]
        [WebGet(UriTemplate = "rt", ResponseFormat = WebMessageFormat.Json)]
        void recreateTable();
    }
}
