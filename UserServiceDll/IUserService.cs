﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace UserServiceDll
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        bool Create(string login, string password);

        [OperationContract]
        [WebInvoke(BodyStyle = WebMessageBodyStyle.WrappedRequest)]
        Guid? Login(string login, string password);

        [OperationContract]
        void Logout(string login);

        [OperationContract]
        ModelDLL.User getUser(Guid sessionUUID);

        [OperationContract]
        bool isSignedIn(Guid sessionUUID);

        [OperationContract]
        void recreateTable();
    }
}
