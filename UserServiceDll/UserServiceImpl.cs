﻿using Microsoft.WindowsAzure.Storage;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using System.Security.Cryptography;
using ModelDLL;
using System.IO;
using System.Xml;
using System.ServiceModel.Web;
using System.Web;

namespace UserServiceDll
{
    public abstract class UserServiceImpl : IUserService, IRestUserService, ModelDLL.IDatabaseConnection
    {
        public static string TableName => User.TableName;

        abstract public SqlConnection SqlConnection { get; }

        abstract public string baseGuiPath { get; }
        
        public UserServiceImpl()
        {
            createTablesIfNotExist();
            //logoutEveryone(); // if we want to use it we must crete only one instance of UserService in starter
        }

        public bool Create(string login, string passwordPlainText)
        {
            try
            {
                if (passwordPlainText == null)
                {
                    return false;
                }
                var hashedPassword = hash(passwordPlainText);
                using (var c = this.SqlConnection)
                {
                    c.Open();
                    var cmd = c.CreateCommand();
                    cmd.CommandText = $"insert into { TableName } (login, password) values(@login, @hash)";
                    cmd.Parameters.Add("@hash", System.Data.SqlDbType.NVarChar, hashedPassword.Length);
                    cmd.Parameters.Add("@login", System.Data.SqlDbType.NVarChar, login.Length);
                    cmd.Parameters["@hash"].Value = hashedPassword;
                    cmd.Parameters["@login"].Value = login;

                    cmd.Prepare();
                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }

        public bool isSignedIn(Guid sessionUUID)
        {
            using (var c = this.SqlConnection)
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = $"select count(*) from { TableName } where sessionUUID='{ sessionUUID.ToString() }'";
                Int32 count = (Int32)cmd.ExecuteScalar();
                return count > 0;
            }
        }

        public Guid? Login(string login, string passwordPlainText)
        {
            try
            {
                var hashedPasswordDb = getPassword(login);
                var hashedPassword = hash(passwordPlainText);

                if (hashedPassword.Equals(hashedPasswordDb) == false)
                    return null;

                var UUID = Guid.NewGuid();
                using (var c = this.SqlConnection)
                {
                    c.Open();
                    var cmd = c.CreateCommand();
                    cmd.CommandText = $"update { TableName } set sessionUUID='{ UUID.ToString() }' where login='{ login }'";
                    cmd.ExecuteNonQuery();
                }
                return UUID;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void Logout(string login)
        {
            using (var c = this.SqlConnection)
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = $"update { TableName } set sessionUUID = null where login='{ login }'";
                cmd.ExecuteNonQuery();
            }
        }

        private void createTablesIfNotExist()
        {
            if (ModelOperations.CheckIfTableExists(TableName, this) == false)
            {
                recreateTable();
            }
        }

        public void recreateTable()
        {
            string createCommand =
                $"CREATE TABLE { TableName } ( " +
                    "id int not null identity," +
                    "login nvarchar(50) not null ," +
                    "password nvarchar(128) not null," +
                    "sessionUUID nvarchar(36)," +
                    "primary key(id)" +
                 ")";

            ModelOperations.RecreateTable(TableName, createCommand, this);
            Create("admin", "admin");
        }

        // get hashed password from db
        private string getPassword(string login)
        {
            using (var c = this.SqlConnection)
            {
                c.Open();
                var cmd = c.CreateCommand();
                cmd.CommandText = $"select password from { TableName } where login='{ login }'";
                return (string)cmd.ExecuteScalar();
            }
        }

        private static string hash(string password)
        {
            SHA512 sha512 = SHA512.Create();
            byte[] bytes = sha512.ComputeHash(Encoding.ASCII.GetBytes(password));
            return System.Text.Encoding.UTF8.GetString(bytes);
        }

        private void logoutEveryone()
        {
            try
            {
                using (var c = this.SqlConnection)
                {
                    c.Open();
                    var cmd = c.CreateCommand();
                    cmd.CommandText = $"update { TableName } set sessionUUID = null";
                    cmd.ExecuteNonQuery();
                }
            } catch (SqlException)
            {
                // TODO
            }
        }

        public User getUser(Guid sessionUUID)
        {
            return ModelDLL.ModelOperations.Select<User>(
                $"select * from { TableName } where sessionUUID = '{ sessionUUID }')",
                this)
                    .FirstOrDefault();
        }

        public bool Create(Stream s)
        {
            var user = ModelOperations.GetFromJson<User>(s).FirstOrDefault();
            return Create(user.Login, user.Password);
        }

        public Guid? Login(Stream s)
        {
            var user = ModelOperations.GetFromJson<User>(s).FirstOrDefault();
            return Login(user.Login, user.Password);
        }

        public void Logout(Stream s)
        {
            var user = ModelOperations.GetFromJson<User>(s).FirstOrDefault();
            Logout(user.Login);
        }

        public User getUser(Stream s)
        {
            var user = ModelOperations.GetFromJson<User>(s).FirstOrDefault();
            return getUser(Guid.Parse(user.SessionUUID));
        }

        public bool isSignedIn(Stream s)
        {
            try
            {
                StreamReader reader = new StreamReader(s);
                string guid = reader.ReadToEnd().Replace("\\", "").Replace("\"", ""); ;
                return isSignedIn(Guid.Parse(guid));
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public Stream getLoginPage()
        {            
            return getStreamFromFile(baseGuiPath + "login.html", GuiDLL.GuiConstants.HTML_TYPE);
        }        

        public Stream getJsFile(string jsFileName)
        {
            return getStreamFromFile(baseGuiPath + "assets\\js\\" + jsFileName, GuiDLL.GuiConstants.JS_TYPE);
        }

        public Stream getCssFile(string cssFile)
        {
            return getStreamFromFile(baseGuiPath + "assets\\css\\" + cssFile, GuiDLL.GuiConstants.CSS_TYPE);
        }

        public Stream getImageFile(string imageFile)
        {
            return getStreamFromFile(baseGuiPath + "images\\" + imageFile, GuiDLL.GuiConstants.IMG_TYPE);
        }

        public Stream getFont1()
        {
            return getStreamFromFile(baseGuiPath + "assets\\fonts\\fontawesome-webfont.woff", "application/font-woff");
        }

        public Stream getFont2()
        {
            return getStreamFromFile(baseGuiPath + "assets\\fonts\\fontawesome-webfont.woff2", "application/font-woff2");
        }

        public Stream getFont3()
        {
            return getStreamFromFile(baseGuiPath + "assets\\fonts\\fontawesome-webfont.ttf", "application/x-font-ttf");
        }

        public Stream getIndexPage()
        {
            return getStreamFromFile(baseGuiPath + "index.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        public Stream getSubPage()
        {
            return getStreamFromFile(baseGuiPath + "subpage.html", GuiDLL.GuiConstants.HTML_TYPE);
        }

        private static Stream getStreamFromFile(string filePath, string contentType)
        {
            var ctx = WebOperationContext.Current.IncomingRequest;
            Console.WriteLine("[" + DateTime.Now.ToLongTimeString() + "] " + ctx.UriTemplateMatch.RequestUri.ToString());
            OutgoingWebResponseContext context =
             WebOperationContext.Current.OutgoingResponse;
            context.ContentType = contentType;
            try
            {
                byte[] file = File.ReadAllBytes(filePath);
                return new MemoryStream(file);
            }
            catch (FileNotFoundException fileNotFoundException)
            {
                return new MemoryStream(ASCIIEncoding.UTF8.GetBytes($"File not found. Description: {fileNotFoundException.Message}"));
            }
        }
    }
}
